# -*- coding: utf-8 -*-
"""
Created on Wed Jan 11 20:25:49 2023

@author: schakraborth
"""
import json
import sys, struct, serial, time, datetime
from pylsl import StreamInfo, StreamOutlet, resolve_stream, StreamInlet
import os
import sys
import time
import serial
import serial.tools.list_ports
from datetime import datetime
import csv
import time

# os.makedirs("assets",exist_ok=True)

participant_code = 2

print('Port searching.....')
ports = serial.tools.list_ports.comports(include_links=False)
for port in ports :
    print('Find port '+ port.device)

port = port.device
#port='COM14' #hardcoding the ports is not a good idea by any means. However, on a system the ports rarely change randomly. So thats fine I guess

def wait_for_ack():
    ddata = ""
    startTime=time.time()
    ack = struct.pack('B', 0xff)
    while ddata != ack:
        ddata = ser.read(1)# print("0x{}02x".format(ord(ddata[0])))
        print('{}'.format(ddata[0]))
        if time.time()-startTime>10:
            print("Ack not from a Shimmer device")
            break #Timeout just in case the device is not Shimmer
    return
ser = serial.Serial(port, 115200)

ser.flushInput()
print("port opening, done.")

# send the set sensors command
ser.write(struct.pack('BBBB', 0x08 , 0x04, 0x01, 0x00))  #GSR and PPG
wait_for_ack()
ser.close()
print("sensor setting, done.")