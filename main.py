# -*- coding: utf-8 -*-
"""
Created on DEC 11 10:01:18 2022

@author: schakraborth
"""
# Author: schakraborth@ethz.ch
# Tested on: Python 3.6
# Disabled the automatic scanning options for shimmer GSR. It was causing my algorithm to take too much time to connect and disconnect to the ports.

# before starting the main please fix shimmer ports. See the code in shimmerTrial.py

#%%

# 1 Required modules import
#--------------------
import threading
import sys
from GSR_to_LSL import GSR_to_LSL
from ET_to_LSL import ET_to_LSL 

# At the current moment this works just with tobii.
# Update: Eye tracking at Winter School is kind of a great oppurtunity to extend this to gazepoint as well.


# 2 Main method
#------------------
def main():
    # 2.1 Set comport to GSR, ECG, and EEG
    comX = 'COM11'     # GSR 
    comY = 'COM14'     # ECG
    #comUSB = 'Com3'         # EEG

    # 2.2 Enable concurrent streaming of data
    gsr = threading.Thread(target=GSR_to_LSL, args=(comX, ))
    gsr1 = threading.Thread(target=GSR_to_LSL, args=(comY, ))
    et = threading.Thread(target=ET_to_LSL,args=(comX,))
    #eeg = threading.Thread(target=EEG_to_LSL, args=(comUSB,))
    
    # 2.3 Start concurrent running of streams
    gsr.start()
    gsr1.start()
    et.start()
    #eeg.start()



if __name__ == "__main__":
    main()
